\documentclass[11pt,letter]{moderncv}
\moderncvtheme[purple]{classic}
\usepackage[margin=0.5in]{geometry}
\usepackage{microtype}

% locale and encoding
\usepackage[utf8]{inputenc}
\usepackage{csquotes}
\usepackage[american]{babel}

% number and unit formatting
\usepackage{siunitx}

% fonts and icons
\usepackage{fontawesome}
\moderncvicons{awesome}
\renewcommand{\familydefault}{\sfdefault}
\usepackage{hologo}

\usepackage{url}

% source code highlighting
% \usepackage{minted}
% \newcommand{\posh}[1]{\mintinline{powershell}{#1}}
\newcommand{\posh}[1]{\texttt{#1}}
\newcommand{\cmd}[1]{\texttt{#1}}

% Macros
\makeatletter % changes the catcode of '@' to 11
\newcommand\etal{et al\@ifnextchar.{}{.\@}}
\newcommand\etc{etc\@ifnextchar.{}{.\@}}
\newcommand\ie{i.e.\@}
\newcommand\eg{e.g.\@}
\makeatother % restores the catcode of '@' to 12
%% Typeset "C++" nicely.
\newcommand{\Cpp}{C\nolinebreak\hspace{-.05em}\raisebox{.4ex}{\tiny\textbf{+}}\nolinebreak\hspace{-.10em}\raisebox{.4ex}{\tiny\textbf{+}}}
%% Typeset "Tikz" according to http://mirror.ctan.org/graphics/pgf/base/doc/generic/pgf/pgfmanual.pdf.
\newcommand\TikZ{Ti\textit{k}Z}

\firstname{Joseph}
\lastname{LaFreniere}
\address{817 Kingswood Avenue}{Richardson, TX 75080}
\phone{\href{tel:+1-865-978-2247}{\texttt{+}1-865-978-2247}}
\email{joseph@lafreniere.xyz}
\extrainfo{%
  \href{https://github.com/lafrenierejm}{\includegraphics[height=0.9em]{./pic/github.pdf} GitHub:lafrenierejm},
  \href{https://gitlab.com/u/lafrenierejm}{\includegraphics[height=0.9em]{./pic/gitlab.pdf} GitLab:lafrenierejm}
}
\begin{document}

% ==============================================================================
\maketitle
\vspace*{-15mm}

\section{Education}
% \cventry{year--year}{Degree}{Institution}{City}{\textit{Grade}}{Description}

\cventry%
{Expected May 2018}
{BS in Software Engineering}
{University of Texas at Dallas}
{}
{\textit{3.48/4.00 cumulative GPA}}
{
  \begin{itemize}
  \item
    Elected officer in Hobson Wildenthal Honors College for 2016--17, 2017--18 academic years.
  \item
    Named President of campus's Linux Users Group spring 2017--spring 2018.
  \item
    Recognized for high school achievements with National Merit Scholarship.
  \end{itemize}
}

% -------------------------------------------------------------------------------
\section{Skills and Competencies}
\cvline{Platforms}{\small{Linux (CentOS, Debian, Void, Alpine, Ubuntu), Windows (10, 8, 7, Vista, XP), MacOS, Android}}
\cvline{Programming}{\small{PowerShell, UNIX shells (POSIX sh, Zsh, Bash), Python, Haskell, C, \Cpp{}, Java, Emacs Lisp, VimL}}
\cvline{Markup}{\small{\hologo{(La)TeX} and PGF/\TikZ{}, Org-mode, Markdown, YAML, nroff, HTML}}
\cvline{Dev Tools}{\small{Git, Subversion, Wireshark (TShark), Emacs, vi and Vim, tmux, GDB (PEDA), Visual Studio, Android Studio}}
\cvline{Deployment}{\small{Ansible, Travis CI, Chocolatey}}

% -------------------------------------------------------------------------------
\section{Work Experience}
% \cventry{Dates}{Title}{Company}{Location}{}{Description}

\cventry
{Summer\\2016} % dates
{Software Engineering Intern} % title
{Northrop Grumman} % company
{Oklahoma City, OK} % location
{} %
{
  \begin{itemize}
  \item
    Collaborated on design, implementation, and benchmarking of Distributed Data Service (DDS) middleware stress testing.
    \begin{itemize}
    \item
      Served as lead architect for code generator needed to produce \num{e5} SLOC of \Cpp{} for multiple middleware configurations.
    \item
      Scripted the generator configuration and DDS benchmarking using PowerShell 4.0.
      The end result was a fully-automated benchmarking suite which was left to run during the nights.
    \item
      Guided teammates in version control best practices including
      \begin{itemize}
      \item
        forking and merging strategies,
      \item
        atomicity of commits,
      \item
        commit history preservation, and
      \item
        commit message conventions and etiquette.
      \end{itemize}
    \end{itemize}
  \item
    Oversaw setup and maintenance of software on isolated test network.
    \begin{itemize}
    \item
      Explored policies for open sourcing generic configurations in a security-conscious environment.
    \item
      Exported lessons learned (\eg{} Subversion server configurations) to the primary development network.
    \end{itemize}
  \item
    Practiced pair programming with another intern to reduce compiler warnings in production builds.
    After one week, \SI{90}{\percent} of the warnings in assigned section of codebase were eliminated.
    \begin{itemize}
    \item
      Informed project lead and original authors of causes of several tenacious warnings after consulting language standards and compiler documentation.
    \item
      Documented the causes of and rationale for not resolving the remaining warnings.
    \end{itemize}
  \end{itemize}
}

\cventry
{Summer\\2013, 14, 15}
{Information Technology Intern}
{Lenoir City Utilities Board}
{Lenoir City, TN}
{}
{
  \begin{itemize}
  \item
    Set up monitoring of hardware and software assets using \href{https://www.viavisolutions.com/en-us/products/observer-analyzer}{VIAVI Observer Analyzer}.
  \item
    Supported major resubnet of production network.
    \begin{itemize}
    \item
      Traced network cables through buildings.
    \item
      Created spreadsheet containing the discovered wall jack-to-punchdown-to-switch mappings.
      Coordinated with my supervisor to update the labels in the switches' configurations.
    \item
      Rerouted and labeled patch cables in server room.
    \end{itemize}
  \item
    Collated technical information for the department's budget request for a fleet of zero clients.
  \item
    Performed quality assurance and testing of zero client configuration.
  \item
    Resolved help desk calls for approximately 75 employees across two buildings
    \begin{itemize}
    \item
      Assembled, imaged, configured, and installed new workstations.
    \item
      Troubleshot in-house and third-party services spanning GIS, OCR, and banking.
    \end{itemize}
  \item
    Scripted registry cleanup of failed Symantec Endpoint uninstallation in PowerShell 2.0.
    The script's source code is available at \href{https://github.com/lafrenierejm/Symantec_Endpoint_Uninstall_Script}{\nolinkurl{github.com/lafrenierejm/Symantec_Endpoint_Uninstall_Script}}.
  \end{itemize}
}

\cventry
{Summer\\2012}
{Groundskeeper}
{Fiesta Farm}
{Knoxville, TN}
{}
{
  \begin{itemize}
  \item
    Fed and watered horses.
  \item
    Mucked horse stalls.
  \item
    Aided construction and installation of new barn doors.
  \end{itemize}
}

% -------------------------------------------------------------------------------
\section{Volunteer Experience}

\cventry
{January 2018\\to Present}
{Information Technology Volunteer}
{Agape Clinic}
{Dallas, TX}
{}
{
  \begin{itemize}
  \item
    Help maintain hardware and software of fleet of laptops used by clinicians and other medical providers.
    \begin{itemize}
    \item
      Customized series of PowerShell scripts to clean and optimize Windows 10 Home installations.
      The fork in use is available at \href{https://github.com/lafrenierejm/Debloat-Windows-10}{\nolinkurl{github.com/lafrenierejm/Debloat-Windows-10}}.
    \item
      Diagnosed and replaced faulty hard drive.
    \end{itemize}
  \item
    Mocked a responsive redesign of clinic's website, \href{https://www.theagapeclinic.org/}{\nolinkurl{theagapeclinic.org}}, using Hugo static site generator.
  \item
    Debugged and refactored a PowerShell script used to generate statistics from the clinic's electronic media records (EMR) system.
  \end{itemize}
}

\cventry%
{Academic\\2015--16} % left margin
{Volunteer Computer Science Teacher} % bold
{} % organization
{Richardson, TX} % location
{} % italicized
{%
  Met with an elementary school student weekly throughout the school year to discuss introductory topics in programming and computer science.
  \begin{itemize}
  \item
    Helped pupil understand rationale for high-level languages.
    \begin{itemize}
    \item
      Created binary math problems for weekly assignments.
    \end{itemize}
  \item
    Used student's imagination of a role-playing game to introduce common imperative language syntax (in this case \Cpp{}).
    \begin{itemize}
    \item
      Steered game mechanics to lead to student's discovery of branches and loops.
    \item
      Guided pupil through reading API documentation during design of textual interface.
    \end{itemize}
  \end{itemize}
}

\cventry
{Summer\\2013, 15}
{ReStore Volunteer}
{Habitat for Humanity}
{Anderson County, TN}
{}
{
  \begin{itemize}
  \item
    Mentored the Executive Director about IT and web technologies
  \item
    Co-redesigned website front-end as responsive layout (Summer 2015)
  \item
    Aided restructuring of ReStore (Summer 2013)
    \begin{itemize}
    \item
      Directed three volunteers in moving merchandise, cleaning store, and refinishing walls
    \end{itemize}
  \end{itemize}
}

% -------------------------------------------------------------------------------
\section{Open Source Projects}

\cventry
{Python 3,\\SQLAlchemy,\\SQLite3,\\argparse}
{Author of CLI Tool for Accessing IETF Publications}
{}
{\href{%
    https://github.com/lafrenierejm/ietf-cli%
  }{%
    \nolinkurl{github.com/lafrenierejm/ietf-cli}%
  }}
{}
{
  Rewrote Paul Hoffman's \href{https://trac.tools.ietf.org/tools/ietf-cli/}{IETF command-line tool} in modern Python 3 with a SQLite backend.
  The database backend allows querying documents by title, author, keyword, or authoring organization.
  Documents that update or obsolete the searched document are automatically fetched.
  \begin{itemize}
  \item
    Wrote scraper for IETF's XML index file to generate metadata for queries.
  \item
    Structured project according to \href{http://as.ynchrono.us/2007/12/filesystem-structure-of-python-project_21.html}{Jean-Paul Calderone's recommendations}.
  \item
    Implemented object-relational mapping using SQLite3 and \href{http://www.sqlalchemy.org/}{SQLAlchemy}.
  \item
    Designed CLI frontend using subcommands to maximize usability and code cohesion.
    The frontend is implemented using the built-in \href{https://docs.python.org/3.5/library/argparse.html}{argparse library}.
  \item
    Provided synchronization commands that use \texttt{rsync} to fetch latest versions of the IETF's publications.
  \end{itemize}
}

\cventry%
{Org-mode,\\Emacs Lisp} % left margin
{Maintainer of Literate Emacs Configuration} % bold
{} % organization
{\href{%
    https://gitlab.com/lafrenierejm/dotfiles/tree/master/.emacs.d
  }{%
    \nolinkurl{gitlab.com/lafrenierejm/dotfiles/tree/master/.emacs.d}}} % location
{} %
{%
  Use \href{http://orgmode.org/worg/org-contrib/babel/}{Babel} to write configuration in the \href{https://en.wikipedia.org/wiki/Literate_programming}{literate programming} paradigm;
  prose explanations and source code implementations of my Emacs's functionality are interleaved in Org-mode files.
  \begin{itemize}
  \item
    Strive for simplicity and readability of source code, attempting to follow and improve upon best practices.
    \begin{itemize}
    \item
      Configuration is split into multiple subdirectories and files for increased cohesion and modularity.
    \item
      Implemented package loading entirely with \href{https://github.com/jwiegley/use-package}{\texttt{use-package} by jwiegley}.
    \item
      Manage packages in a purely functional way with \href{https://github.com/raxod502/straight.el}{raxod502's \texttt{straight.el} library}.
    \end{itemize}
  \item
    Posted excerpts from my configuration as well-received answers on the Stack Exchange network, \eg{} \href{https://stackoverflow.com/a/46937891/8468492}{\nolinkurl{stackoverflow.com/a/46937891/8468492}}.
  \end{itemize}
}

\cventry%
{VimL} % left margin
{Maintainer of RFC 3676 Vim Plugin} % bold
{} % organization
{\href{https://gitlab.com/lafrenierejm/vim-format-flowed}{\nolinkurl{gitlab.com/lafrenierejm/vim-format-flowed}}}
{} % italicized
{
  Wanted to dynamically set Vim's \texttt{formatoptions} and tab settings based on the location of the cursor in a buffer in order to bring support for \href{https://tools.ietf.org/html/rfc3676}{RFC 3676} to Vim's mail filetype.
  At the time of looking, the only attempt at a VimL implementation of RFC 3676 existed as \href{https://groups.google.com/d/msg/vim_use/r3TdW9G9ms4/s-Jr3BpcnvUJ}{a script in the Vim user Google Group}.
  This project started as a refactorization of that existing script, but now uses none of the original source code.
  \begin{itemize}
  \item
    Rewrote the regular expressions used to detect headers, signatures, quotations, and patches.
  \item
    Expanded the set options to account for automatic reformatting, auto-wrapping paragraphs, the effects of trailing whitespace, and the behavior of tab characters.
  \item
    Packaged the script for use with modern Vim plugin managers.
  \end{itemize}
}


\cventry%
{Ansible,\\YAML} % Left margin
{Author of Ansible Roles for Void Linux} % Bold
{} % Organization
{\href{https://gitlab.com/void-linux-ansible-roles}{\nolinkurl{gitlab.com/void-linux-ansible-roles}}} % Location
{} % Italicized
{
  Published Ansible roles to manage client machines with Void Linux's package manager.
  \begin{itemize}
  \item
    Current roles install and configure development tools for C, \Cpp{}, Python 3, Haskell, and Rust.
  \item
    Roles are tested in provisioning my primary development machine.
  \item
    Experimental support for pulling dotfile repositories per-user.
  \end{itemize}
}

\cventry%
{POSIX sh,\\Travis CI,\\nroff} % Left margin
{Maintainer of CLI Utility to Modify Filename Extensions} % Bold
{} % Organization
{\href{https://github.com/lafrenierejm/chext/}{\nolinkurl{github.com/lafrenierejm/chext}}} % Location
{} % Italicized
{
  Forked a script to change filename extensions.
  The original script was written in Bash and only allowed \textit{changing} extensions.
  \begin{itemize}
  \item
    Rewrote the script in POSIX-compliant sh for better portability.
  \item
    Added abilities to add and remove extensions.
  \item
    Introduced unit tests and a Travis CI script to discover bugs.
  \item
    Endorsed by original author as having written the canonical version of the utility.
    Received interest from users in packaging for popular repositories, e.g.\@ Homebrew.
  \end{itemize}
}

\cventry
{PowerShell}
{Author of PowerShell Cmdlet for Creating Symlinks}
{}
{\href{https://github.com/lafrenierejm/New-Link}{\nolinkurl{github.com/lafrenierejm/New-Link}}}
{}
{
  \begin{itemize}
  \item
    Wrote cmdlet wrapper around \posh{New-Item} to ease creation of symlinks in PowerShell.
  \item
    Validated the two required arguments with the built-in \posh{ValidateSet} and \posh{ValidateScript}.
  \item
    Exported the alias \posh{mklink} to reduce friction for users coming from cmd.exe.
  \end{itemize}
}

% -------------------------------------------------------------------------------
\section{Relevant Coursework}

\cventry%
{Fall\\2017}
{Computer Networks}
{SE 4390}
{University of Texas at Dallas}
{}
{
  Syllabus available at \href{https://dox.utdallas.edu/syl64728}{\nolinkurl{dox.utdallas.edu/syl64728}}.
  The stated learning objectives of the course were to
  \begin{itemize}
  \item
    understand the need and structure of the OSI and TCP/IP network models,
  \item
    design and evaluate methods for the framing messages in transmission media,
  \item
    analyze and evaluate different error detection schemes,
  \item
    understand and evaluate multiple-access protocols,
  \item
    design and evaluate routing protocols,
  \item
    design and evaluate flow control and congestion control protocols,
  \item
    understand the issues in inter-network design,
  \item
    understand the various Internet protocols (TCP/IP), and
  \item
    specify networking protocols and carry out network programming.
  \end{itemize}
  The course capstone was the development of a simple software router written in C.
}

\cventry
{Fall\\2017}
{Computer and Network Security}
{SE 4393}
{University of Texas at Dallas}
{}
{
  Syllabus available at \href{https://dox.utdallas.edu/syl64731}{\nolinkurl{dox.utdallas.edu/syl64731}}.
  The stated learning objectives of the course were to understand
  \begin{itemize}
  \item
    fundamental security concepts;
  \item
    common threats and vulnerabilities of computer systems and networks;
  \item
    algorithms and practices of symmetric key cryptography;
  \item
    algorithms and practices of public key cryptography;
  \item
    principles and practices of authentication methods;
  \item
    principles of secured protocols and their practices;
  \item
    principles and practices of networks defense; and
  \item
    techniques, principles and practices of computer systems defense.
  \end{itemize}
  Hands-on experience consisted of
  \begin{itemize}
  \item
    carrying out a buffer overflow attack on a mock C program,
  \item
    comparing the effectiveness of various encryption techniques by encrypting, corrupting, and decrypting a simple image with multiple encryption schemes,
  \item
    intercepting, analyzing, and tampering with plaintext HTTP traffic.
  \end{itemize}
  The design evolution of Kerberos was discussed in depth, though we were never asked to configure a working krb5 domain.
  Two brief papers were written to offer high-level explanations of elliptic curve cryptography and WPA2 KRACK attacks.
}

\cventry%
{Spring\\2016}
{Topics in Enterprise Networking}
{Pass-Fail Honors Course}
{University of Texas at Dallas}
{}
{
  \begin{itemize}
  \item
    Lightweight Directory Access Protocol (LDAP) use and syntax
  \item
    Overview of networked printing in a *NIX domain
  \item
    Introduction to configuration management with Ansible
  \end{itemize}
}

\cventry%
{Spring\\2016} % left margin
{Operating Systems Concepts}
{SE 4348}
{University of Texas at Dallas} % location
{}
{%
  Syllabus available at \href{https://dox.utdallas.edu/syl52373}{\nolinkurl{dox.utdallas.edu/syl52373}}.
  \begin{itemize}
  \item
    Discussion of operating system topics with emphasis on UNIX implementations of
    \begin{itemize}
    \item
      process and memory management,
    \item
      multithreading, and
    \item
      I/O scheduling.
    \end{itemize}
  \item
    Wrote C and Java programs to implement above concepts
  \end{itemize}
}

\cventry%
{Spring\\2016}%
{C/\Cpp{} Programming in a UNIX Environment}
{SE 3376}
{University of Texas at Dallas}
{}
{
  Syllabus available at \href{https://dox.utdallas.edu/syl48713}{\nolinkurl{dox.utdallas.edu/syl48713}}.
  \begin{itemize}
  \item
    Made use of forks, pipes, and other UNIX structures in both C and \Cpp{}.
  \item
    Wrote applications to interface with sqlite3 databases.
  \end{itemize}
}

\cventry%
{Fall\\2015}
{Data Structures and Intro to Algorithmic Analysis}
{SE 3345}
{University of Texas at Dallas}
{}
{
  Syllabus available at \href{https://dox.utdallas.edu/syl60924}{\nolinkurl{dox.utdallas.edu/syl60924}}.
  \begin{itemize}
  \item
    Created classes and test drivers for basic data structures (queues, trees, etc.).
  \item
    Used \TikZ{} package in \LaTeX{} to draw graphical representations of concepts.
  \end{itemize}
}

\cventry%
{Spring\\2015}%
{Computer Architecture}
{SE 3340}
{University of Texas at Dallas}
{}
{
  Syllabus available at \href{https://dox.utdallas.edu/syl43579}{\nolinkurl{dox.utdallas.edu/syl43579}}.
  The course capstone project was a team project to write a hang man game in MIPS assembly.
}

\cventry%
{Spring\\2015}
{Mathematical Foundations of Software Engineering}
{SE 3306}
{University of Texas at Dallas}
{}
{
  Syllabus available at \href{https://dox.utdallas.edu/syl46560}{\nolinkurl{dox.utdallas.edu/syl46560}}.
  \begin{itemize}
  \item
    Computed prime factorization of large numbers with modified Sieve of Eratosthenes
  \item
    Calculated greatest common divisor of two unsigned integers with Euclidean Algorithm
  \item
    Built application to write step-by-step exponential modulation solver in \TeX{}
  \end{itemize}
}

% ===============================================================================
\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End: